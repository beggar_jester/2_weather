import java.util.Scanner;

public class Weather {
    public static void main(String[] args) {
        System.out.println("Enter air temperature outside:");
        int airTemperature = new Scanner(System.in).nextInt();
        int windSpeed = -1;
        while (windSpeed < 0 || windSpeed > 12) {
            System.out.println("Enter the wind speed on the Beaufort scale (integer 0-12):");
            windSpeed = new Scanner(System.in).nextInt();
        }
        System.out.println("There is rain outside? (leave blank to False, True else)");
        boolean rainPrediction = true;
        String prediction = new Scanner(System.in).nextLine();
        if (prediction.isBlank()) {
            rainPrediction = false;
        }

        System.out.println(yourPocketAdvice(airTemperature, windSpeed, rainPrediction));
    }

    private static String yourPocketAdvice(int temperature, int beaufortScore, boolean rain) {
        if ((temperature > 15 && temperature < 24) && (beaufortScore < 7) && (!rain)) {
            return "Time to walk a raccoon!";
        } else if (rain) {
            return "Don't forgot an umbrella.";
        } else if (temperature <= 15) {
            return "Don't forgot a hat.";
        } else if (temperature >= 24) {
            return "Don't forgot take off your coat.";
        }else {
            return "Time to stay home!";
        }
    }

}
